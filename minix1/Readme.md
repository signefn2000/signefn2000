# MiniX1 // Party-night-and-day-house-💃

please run the code here: https://signefn2000.gitlab.io/signefn2000/minix1 

please view the full reposetory:
https://gitlab.com/signefn2000/signefn2000/-/tree/main/minix1?ref_type=heads


![billede](./house2.png)


### **Reflections**
In this project, I chose to play around with the random function in p5.js. I've been watching tutorials on The Coding Train's YouTube channel to get a better handle on it, and it's been a huge help in shaping my final miniX1 project.

I used the random function on the RGB values of the colors and for the size of the door.Originally, I was gonna slow down the frame rate to make the house easier on the eyes. But when I added a sun to the scene, it didn't really fit with a slower frame rate. 

For the sun i tried to make it look a bit like a sunrise. I ended up making a mathematical function that determines the vertical position (y-coordinate) of the circle based on the horizontal position (mouseX) with a degree of curvature controlled by the 'exponent' variable.  I utilized the map function to dynamically adjust the blue component of the RGB color code based on the width of the screen.

For a bit of amusement, I also implemented a mousePressed function that reveals a smiley face. I find it entertaining that you need to tap the screen multiple times to unveil what it actually is. Upon each press, a cheerful smiley face pops up, spreading a bit of joy.

Hope you enjoyed the party 💃💃💃

(FYI - runs best in Google Chrome)


### **References**


[The Coding Train on map function](https://www.youtube.com/watch?v=nicMAoW6u1g)
<br>
[The Coding train on random function](https://www.youtube.com/watch?v=POn4cZ0jL-o&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=11)
<br>
[The Coding Train house](https://editor.p5js.org/codingtrain/sketches/HGq_S0Z5H)
