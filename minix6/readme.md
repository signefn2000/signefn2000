# The fox stole all my money... (minix6)

**please run code in Google Chrome Browser:**

**please run the code here:** https://signefn2000.gitlab.io/signefn2000/minix6

**please view the full reposetory:**


![billede](./minix6.png)




### **Reflections**

– **how does/do your game/game objects work?**

The fox is controlled by the player and moves around the screen. The fox collects wallets while avoiding collisions with its own tail or the game boundaries. The length of the fox's tail increases every time it collects a wallet, and the game ends if the fox collides with itself or goes out of bounds. 

– **how you program the objects and their related attributes, and the methods in your game.**

I have defined a class called Fox to represent the tFox object. This class includes attributes such as the fox's body segments (body), direction (xdir and ydir), and the length of its body (lengthOfBody). Methods within the Fox class handle tasks such as updating the fox's position (update()), detecting collisions (endGame()), and picking up wallets (eat()). Additionally, the show() method is responsible for rendering the fox and its tail on the screen.


– **Draw upon the assigned reading, are the characteristics of object-oriented programming and the wider implications of abstraction?**


– **your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?**

- 

```ruby
 
//

```






### **References**

[Coding train - coding challenge#3 Snake game](https://www.youtube.com/watch?v=AaGK-fj-BAM)

