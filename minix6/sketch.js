
let tFox
let structure = 20;
let wallet;
let resolution=10
let foxpic;
let walletpic;
let dollarpic
let forestpic
let startGameMessageDisplayed = true; // Flag to track whether the start game message is displayed

let scl = 20;

let w 
let h 
let song;
let songName = false; // Flag to track whether text should be displayed

function preload(){
 foxpic = loadImage("fox.png");
 walletpic = loadImage("wallet.gif");
 dollarpic = loadImage("dollar5.png")
 forestpic = loadImage("forest4.png")

}


function setup(){
createCanvas(500, 500);

w=floor(width/resolution)
h=floor(height/resolution)
frameRate(15)
tFox= new Fox()
walletLocation();
song = loadSound('ræv.mp3', loaded);

}
function loaded() {
    console.log('Sound file is loaded.');
  }
  

function walletLocation(){
    let x = floor(random(w))
    let y = floor(random(h))
    //if it is divisible by 2
    wallet = createVector(x,y)
     
}

function draw (){
    background(forestpic)
    scale(resolution)


    tFox.update();
    tFox.show();

    fill(255,0,0)
    image(walletpic,wallet.x,wallet.y,5,5);


    if (startGameMessageDisplayed) {
        displayStartGameMessage();
    } else {
        if (tFox.eat(wallet)) {
            walletLocation();
        }}

    if (tFox.endGame()){
        print('end game')
        background(0)   
        noLoop()
        textAlign(CENTER, TOP);
    textSize(5);
    fill(255,255,0); 
    text('GAME OVER', 25,20);
    textSize(2);
    text('You got too greedy', 25,25);
    textSize(1);
    text('Please refresh to start again', 25,40);
    }

}

function displayStartGameMessage() {
    textAlign(CENTER, CENTER);
    textSize(2);
    fill(255);
    text('Press arrows to start the game', 25,10);
}

function keyPressed() {
    if (!song.isPlaying()) { // Check if the song is not already playing
        song.play(); // Play the song
    }
    startGameMessageDisplayed = false; // Hide the start game message

    if (keyCode === UP_ARROW && tFox.ydir !== 1) {
        tFox.setDir(0, -1);
    } else if (keyCode === DOWN_ARROW && tFox.ydir !== -1) {
        tFox.setDir(0, 1);
    } else if (keyCode === RIGHT_ARROW && tFox.xdir !== -1) {
        tFox.setDir(1, 0);
    } else if (keyCode === LEFT_ARROW && tFox.xdir !== 1) {
        tFox.setDir(-1, 0);
    }
}



// Function to start displaying the text
function printSongName() {
    songName = true;
  }
