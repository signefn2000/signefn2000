class Fox {

    constructor() { 
       this.lengthOfBody=1
       this.body = [];
       this.body[0] = createVector(floor(w/2),floor(h/2));
       this.xdir = 0;
       this.ydir = 0;
       this.len = 0;
    }
       
    setDir(x, y) {
       this.xdir = x;
       this.ydir = y;
    }
       
       
    update() { //save where it was and move it 
       let head = this.body[this.body.length-1].copy();
       if (this.body.length == this.lengthOfBody)
       {
        this.body.shift();
       }
       head.x += this.xdir;
       head.y += this.ydir;
       this.body.push(head);
       
    }
   
   
    endGame() {
       let x = this.body[this.body.length - 1].x;
       let y = this.body[this.body.length - 1].y;
       // Check if the fox's head position coincides with any of its body segments
       for (let i = 0; i < this.body.length - 1; i++) {
           let part = this.body[i];
           if (part.x === x && part.y === y) {
               console.log("Fox collided with its own tail.");
               return true;
           }
       }
       // Check if the fox is out of bounds
       if (x > w - 1 || x < 0 || y > h - 1 || y < 0) {
           console.log("Fox is out of bounds.");
           return true;
       }
       return false;
   }

 

eat(position) { //when fox picks up wallet
    let head = this.body[this.body.length - 1];
    if (head.x <= position.x+1 && head.x >= position.x-1 && 
        head.y <= position.y+1 && head.y >= position.y-1) {
        this.lengthOfBody++;
        return true;
    }
    return false;
}
   
   
    show() {
        let foxSize = 5; // Adjust the size of fox

        for (let i = 0; i < this.body.length-1; i++) { //display body as 
            console.log(this.body.length)
            let x = this.body[i].x;
            let y = this.body[i].y;
            image(dollarpic, x, y, foxSize, foxSize);
       }
       let head = this.body[this.body.length - 1]; //display front as fox
       image(foxpic, head.x, head.y, foxSize, foxSize);
           // Display length of tail
    textSize(1);
    fill(255);
    text("Money stolen: " + (this.lengthOfBody  - 1), 5, 3);

   }
   
   }