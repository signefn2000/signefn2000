


# Flight From the City... (minix5)

**please run the code here:** https://signefn2000.gitlab.io/signefn2000/minix5



![billede](./minix5.png)

My minix5 was inspired by the song Flight from the city by Jöhan Jöhannsson from this album:

![billede](./Skærmbillede_2024-04-05_kl._11.20.12.png)


### **Reflections**


– **What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?**

rule 1. The circle must be pulsating using the noise function
rule 2. if the song is playing the album title should appear 

My program is designed to be a calming peaceful piece of generative art. i want the viewer to dissapear in the song and the pulsating circle just repeating and repeating in the same way. 

here you can see how i've used the for loop and coditional statement - avoiding making it interactive by using the if statement to activate the song. In my code i have also used the noise function to make the pulsting effect smooth. 

```ruby

//using a for loop
   for (let r = radius; r > 0; r -= 3) {
    let blurAlpha = map(r, radius, 0, alpha, 0);
    fill(0, blurAlpha);
    noStroke();
    ellipse(centerX, centerY, r );
  
  }

//Using an if statement

// Display text at the top of the page
   if (songName) {
    textAlign(CENTER, TOP);
    textSize(32);
    fill(0); 
    textFont(myFont);
    text('JÓHANN JÓHANNSSON\n Orphée', width / 2, 20);
    
    // Start playing the song if the "love" text is displayed
    if (!song.isPlaying()) {
      song.play();
      console.log('sound playing')
    }

//Using noice function
    let alpha = map(noise(noiseOffset), 0, 1, 0, 40); //generates a noise value from 0-1 and maps it

```


– **What role do rules and processes have in your work?**

Typycally i create my programs by beginning to code. i have discovered that i often can feel limited when i start working from a very worked out idea, because programming still is new to me. likewise i feel like i can get to focused on the plan i have in my head that i miss other great ideas or other fortunate mistakes. I was struggeling a bit with the song as some browsers only allow music after user interactivity. I found a loophole though by playing the song if the title appears. Though the program still follows my rules, the coding is now structured the other way around.

– **Draw upon the assigned reading, how does this MiniX help you to understand the
idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?**

What i found especially intresting in this chapter was seeing the exampels of generative art Especially th.work Wall Drawing #289 was very interesting as it touches upon a philosophical question - can a computer be creative? The computer were only given three simple instruction and made the artpiece only based of of these. Now does the computers handeling of these instructions make it creative or is it just completely  random? 


ps. if song is not playing... please restart browser

