
let radius;
let noiseOffset = 0; //controle pulse
let song;
let songName = false; // Flag to track whether text should be displayed
let myFont; // Variable to hold the loaded font


//----------------SETUP---------------------
function setup() {
  createCanvas(windowWidth, windowHeight);

  song = loadSound('song.mp3', loaded);
  myFont = loadFont('FiraCode.ttf');


  // Callback function called when sound file is loaded
function loaded() {
  console.log('Sound file is loaded.');
}

  // Call printSongName() after a delay (in milliseconds)
  setTimeout(printSongName, 3000); // Start displaying text and playing song after 3 seconds
}
//----------------SETUP END---------------------



//----------------DRAW---------------------
function draw() {
  background(255);
  // Calculate the alpha value based on noise
  let alpha = map(noise(noiseOffset), 0, 1, 0, 40); //generates a noise value from 0-1 and maps it

  radius = 500;
  let centerX = width /2 
  let centerY = height/2 +20


  // Draw a blurred circle with a fading edge
  for (let r = radius; r > 0; r -= 3) {
    let blurAlpha = map(r, radius, 0, alpha, 0);
    fill(0, blurAlpha);
    noStroke();
    ellipse(centerX, centerY, r );
  
  }

  // Increase the noise offset to make the pulsating effect
  noiseOffset += 0.009;
  

  // Draw a blurred line
  for (let i = 0; i < 20; i++) { //starer ved 0 øges for hver gentagelse indtil den rammer 19 fordi 19<20
    let blurAlpha = map(i, 0, 20, alpha, 0); // Map alpha based on iteration - transparecy. alpha er maks gennemsigtighed og 0 er fuld opacitet
    stroke(0, blurAlpha); // Set stroke color with mapped alpha
    strokeWeight(7); 
    line(0, windowHeight / 2 + i+20, windowWidth, windowHeight / 2 + i+20); // Draw line
  }


   // Display "love" text at the top of the page
   if (songName) {
    textAlign(CENTER, TOP);
    textSize(32);
    fill(0); 
    textFont(myFont);
    text('JÓHANN JÓHANNSSON\n Orphée', width / 2, 20);
    
    // Start playing the song if the "love" text is displayed
    if (!song.isPlaying()) {
      song.play();
      console.log('sound playing')
    }
  }
}
//----------------DRAW END---------------------


// Function to start displaying the text
function printSongName() {
  songName = true;
}

//RESIZE WINDOW
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}