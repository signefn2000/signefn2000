
//DEFINE VARIABLE - shapeTypes
let shapeTypes = ["circle", "triangle", "rect"]; //array
let currentShapeIndex = 0; // starts with first shape

//DEFINE VARIABLE - skinColors 
let skinColors = [
  '#FFDE00',
  '#FFDAB9', // PeachPuff
  '#FFC68C', // LightSalmon
  '#8B4513', // SaddleBrown
  '#FFE4C4', // Bisque
  '#FFDEAD', // NavajoWhite
  '#F4A460', // SandyBrown
  '#CD853F', // Peru
  '#D2691E', // Chocolate
  '#A0522D', // Sienna
  '#8B4513', // DarkSaddleBrown
];//array
let currentColorIndex = 0; // Start with the first skin color

//DEFINE VARIABLE - mouthShapes
let mouthShapes = ["smile", "neutral", "frown", "extraHappy"];//aaray
let currentMouthIndex = 0; // Start with the first mouth shape





function setup() {
 
  //SETUP CANVAS
  createCanvas(570, 630);


  // CREATE BUTTON - shapeButtun
  let shapeButton = createButton("Next Shape");
  shapeButton.position(70, 100); //placement
  //styling of button 
  //sets property to value
  //CSS decleration style
  shapeButton.style('background-color', '#C79AF1'); 
  shapeButton.style('border', '2px solid rgb(175, 93, 244)');
  shapeButton.style('border-radius', '10px'); //
   // sets up an eventlistener (mousePressed). It listens for a click on shapeButton. 
   //When this happens it calls the function passed to it like an argument - "changeShape"
  shapeButton.mousePressed(changeShape); 

  

 // CREATE BUTTON - skinColor
  let skinButton = createButton("Change Skin Color");
  skinButton.style('background-color', '#C79AF1'); 
  skinButton.position(175, 100);
  //style button

  skinButton.style('background-color', '#C79AF1'); 
  skinButton.style('border', '2px solid rgb(175, 93, 244)');
  skinButton.style('border-radius', '10px'); //
   // sets up an eventlistener (mousePressed). It listens for a click on skinButton. 
   //When this happens it calls the function passed to it like an argument - "changeSkinColor"
  skinButton.mousePressed(changeSkinColor);


  // CREATE BUTTON - mouthButtun
  let mouthButton = createButton("Change Mouth")
  mouthButton.position(330, 100);
  mouthButton.style('background-color', '#C79AF1'); 
  mouthButton.mousePressed(changeMouth)
  mouthButton.style('border', '2px solid rgb(175, 93, 244)');
  mouthButton.style('border-radius', '10px'); //
}



function draw() {

// DRAW BACKGROUND
// defines it here so that it covers the previous shape
  background(230, 215, 244); 



//FLOWERS
fill(250,250,0)
circle(100,550,12)

fill(200,200,200)
ellipse(100, 534, 15, 20)
ellipse(100, 567, 15, 20)
ellipse(84, 551, 20, 15)
ellipse(116, 551, 20, 15)

fill(230,230,0)
circle(200,550,12)
fill(100,200,200)
ellipse(200, 534, 15, 20)
ellipse(200, 567, 15, 20)
ellipse(184, 551, 20, 15)
ellipse(216, 551, 20, 15)


fill(250,250,0)
circle(300,550,12)

fill(200,200,200)
ellipse(300, 534, 15, 20)
ellipse(300, 567, 15, 20)
ellipse(284, 551, 20, 15)
ellipse(316, 551, 20, 15)

fill(230,230,0)
circle(400,550,12)
fill(100,200,200)
ellipse(400, 534, 15, 20)
ellipse(400, 567, 15, 20)
ellipse(384, 551, 20, 15)
ellipse(416, 551, 20, 15)


fill(250,250,0)
circle(500,550,12)
fill(200,200,200)
ellipse(500, 534, 15, 20)
ellipse(500, 567, 15, 20)
ellipse(484, 551, 20, 15)
ellipse(516, 551, 20, 15)


//SHAPES
//Retrieves the current shape from the shapeTypes array based on the currentShapeIndex variable.
// Retrieves the shape name corresponding to the current index.
  let currentShape = shapeTypes[currentShapeIndex];


// Set fill color of shape based on current skin color
fill(skinColors[currentColorIndex]);

// Draw the selected shape
if (currentShape === "circle") {
  circle(200, 250, 200); 
} else if (currentShape === "triangle") {
  triangle(80, 350, 200, 150, 320, 350); 
} else if (currentShape === "rect") {
  rect(100, 150, 200, 200); 
}


//MOUTH

//fill color of mouth
fill(200,0,0)
  // Draw the selected mouth shape
  let currentMouth = mouthShapes[currentMouthIndex];


  if (currentMouth === "smile") {
    // Draw a smile shape
    //arc(x, y, w, h, start, stop)
    arc(200, 270, 70, 30, 0, PI);
  } else if (currentMouth === "neutral") {
    // Draw a neutral mouth shape
    //line(x1, y1, x2, y2)
    line(160, 270, 240, 270);
  } else if (currentMouth === "frown") {
    // Draw a frown shape
    // arc(x, y, w, h, start, stop)
    line(160, 285, 240, 265);
  } else if (currentMouth === "extraHappy") {
  // draw ekstra happy
  // arc(x, y, w, h, start, stop)
  arc(200, 270, 100, 60, 0, PI);
}


//MAKE EYES
fill(0)
circle(165,240,20)
circle(235,240,20)


//TEXT ON PAGE
fill(142,116,167)
textSize(30);
text('Modify the smiley so it feels like you;)', 30, 70);



}

//MAKE BUTTON GO TO NEXT ELEMENT
//adds one to the current index and makes sure it is cycling throug and not ending at last element in index
//defined in global scope - can be used anywhere in the code 
//% - makes sure it cycles through by taking the modulo - calculating the remainder

//shapes
function changeShape() {
  currentShapeIndex = (currentShapeIndex + 1) % shapeTypes.length;
}
//skinColor
function changeSkinColor() {
  currentColorIndex = (currentColorIndex + 1) % skinColors.length;
}
//mouth
function changeMouth() {
  currentMouthIndex = (currentMouthIndex + 1) % mouthShapes.length;
}