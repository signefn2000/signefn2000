# Minix2 // Muliple-emojis-in-one

please run the code here: https://signefn2000.gitlab.io/signefn2000/Minix2

please view the full reposetory:
https://gitlab.com/signefn2000/signefn2000/-/tree/main/minix1?ref_type=heads


![billede](./emoji.png)


### **Reflections**
Describe your program and what you have used and learnt.

The Emoji generator is a simple program that allows users to create emoji-like faces with different shapes, skin colors, and mouth expressions. This way you can modify the classical emoji so it matches your look, mood, personality ect. I created buttons to change the shape of the face, the skin color, and the mouth expression, offering users a place explore different combinations and create their own unique emoji.
In creating this i learnd a lot of new functions in p5.js. i experimentet with creating buttons and adding functions to it - making the program interactive for the user. Likewise i used a lot of time getting to know the different shapes, lines and curves. I found that i takes quite a while in p5 to get the placement of these absolutly perfect.

how would you put your emoji into a wider social and cultural context that
concerns a politics of representation, identity, race, colonialism, and so on? (Try to think through the assigned reading and your coding process, and then expand that to your own experience and thoughts - this is a difficult task, you may need to spend some time thinking about it).

On this weeks project i based my product on the reading and aspecially these lines from the book:

"The challenge then is to rethink normative geometries, to turn them upside down and inside out. Herein lies the purpose of the chapter, to escape geometric overcoding and to develop alternatives. Having fun with programming in this sense is the ability to modify forms and to diverge from established rules." (page 68)

The use of emojis, particularly smiley faces, has become an ingrown part of our culture and way of comunicating. However, emojis can also carry social and cultural connotations related to representation, identity, race, and colonialism.

In this program, the inclusion of various skin colors aims to promote diversity and inclusivity, allowing users to personalize the emoji to better represent themselves. Additionally, the ability to choose different mouth expressions acknowledges the complexity of human emotions.

By allowing users to customize the emoji's appearance, the program encourages reflection on the significance of representation in digital communication and the importance of inclusivity and cultural sensitivity in emoji design. 


- Signe 🫡