# I know you... (minix4)

**please run the code here:** https://signefn2000.gitlab.io/signefn2000/minix4

**please view the full reposetory:**


![billede](./img.png)

I struggeld a bit to get the uploaded file to work properly. If your having trouble heres a short video of me using the program.

[![video](Code.mov)]


### **Reflections**



- **A short description of my work as if i was going to submit it to the transmediale festival.**

"I know you" is a digital project that captures various forms of human interaction using real-time facial tracking, audio analysis, mouse-tracking and geolocation data. Participants are enveloped in an immersive environment where their actions directly influence the installation's response, revealing the extent to which digital platforms can effortlessly collect highly personal information. By prompting reflection on the pervasive "capture all" atitude of tech giants and the the pervasive reach of surveillance capitalism, this interactive experience encurage critical discourse on privacy, technology, and the evolving digital society.

- **Notes on my program and what i have used and learnt.**

Through this project, I learned about real-time data capture and processing using p5.js. I especially struggeled a lot to get the headMovement working. Therefore i learnt a lot by experimenting with especially arrays ending up calculating the movement like this.   

```ruby
  let facePositions = ctracker.getCurrentPosition(); 

    if (facePositions.length) { 
        if (frameCount % 2 == 0) {
        getCurrentNosePos(noseArray, facePositions[33][0], facePositions[33][1]) }

        movement = calculateMovement(noseArray, facePositions); 
    }

//

    function calculateMovement(previousPos, currentPos) {
   
    currNoseX = currentPos[33][0]; 
    currNoseY = currentPos[33][1]; 

    let noseMovement = dist(previousPos[0][0], previousPos[0][1], currNoseX, currNoseY);
      
    return noseMovement
      }
```

Here i t track facial movement by first retrieving the current positions of facial landmarks of the nose, particularly the nose, using the p5 face tracking library. If facial landmarks are detected, it updates the position of the nose every other frame and calculates the movement between frames by measuring the distance between the current and previous positions of the nose.


In the project i have alsoe used the comunity library "p5.geolocation" by Ben Moren. This is what makes me able to find the users current geolocation. I have found help for using this by looking at the comunitys code on p5s online editor (look in refrences).

- **How my program and thinking address the theme of “capture all.” – What are the cultural implications of data capture?**

> "Surveilance capitalists know everything *about us*, whereas there operations are designed to be unknowable *to us*."  (Shoshana Zuboff, The age of surveillance capitalism p. 11 )

In designing my program my focus has been on making the invisible visible. I want to show the users exactly how much information you can gather just by writing a few lines of simple code. As professor Shoshana Zuboff claims we live in the age of surveillance capitalism. Where our data and personal information are claimed as fre raw material for hidden comercial practices of extraction predection and sales. Without us knowing or in many cases even able to deny the claim of our data. In many aspects we are in the dark. We are not even able to fathom the scale that the survailance capitalism has reched. Therefor it was especially important for me to highlight just how personal the things they might know about you can be - That sites might be able to detect what is happening beyond the hardware inputs like mousepad or keyboard and detect things like our voice or facial expressions. 




### **References**

[Sample code - Geolocation](https://editor.p5js.org/shiffman/sketches/HkQ8kMdee)

Zuboff, Shoshanna (2019). The Age of Surveillance Capitalism: The Fight for a Human Future at the New Frontier of Power. Profile Books.