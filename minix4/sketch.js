
//---------------------------------------------------------------------------
//GLOBAL VARIABLES
//---------------------------------------------------------------------------

let microphone;
let capture;
let ctracker;

let noseArray = [[0, 0]] //starting with an empty array
let movement;
let currNoseX;
let currNoseY;

let prevMouseX;
let prevMouseY;

//---------------------------------------------------------------------------
//SETUP
//---------------------------------------------------------------------------

function setup() {

//CANVAS
createCanvas(windowWidth, windowHeight);
background(0);
frameRate(25);

// AUDIO CAPTURE
  // Audio capture
  microphone = new p5.AudioIn();
  microphone.start();

// CAMERA CAPTURE
    capture = createCapture(VIDEO);
    capture.size(windowWidth, windowHeight);
    capture.hide(); //make normal camera disaopear 

//SETUP FACE TRACKER
    ctracker = new clm.tracker();
    ctracker.init(pModel);
    ctracker.start(capture.elt);




//MOUSE positions
   prevMouseX = mouseX;
   prevMouseY = mouseY;

}
//SETUP END


//------------------------------------------------------------------------------
//DRAW
//------------------------------------------------------------------------------

function draw() {


//CENTER TEXT
    textSize(70);
    fill(255)
    textAlign(CENTER,CENTER)
    text('i know you.',windowWidth/2,windowHeight/2-50)
    //reset font  
    fill(255);
    textSize(32);
    
//PRINT WHEN SPEAKING
   let vol =microphone.getLevel();

   if (vol > 0.003) {
       text("you're speaking!", windowWidth/2,windowHeight/2-100);}
       console.log("Volume Level:", vol);

//GET FACEPOSITION AND DETECT MOVEMENT

    let facePositions = ctracker.getCurrentPosition(); // stores current positions of face detected by face tracker and stores  in facePositions array.

    if (facePositions.length) { //checks if the array contains any elements == face landmarks are detected 
        if (frameCount % 2 == 0) {
        getCurrentNosePos(noseArray, facePositions[33][0], facePositions[33][1])
        }
        movement = calculateMovement(noseArray, facePositions); //Calculates the movement based on the current and previous positions stored in noseArray and the facePositions array. The result is stored in the movement variable.
    }
    
//PRINT TEXT WHEN MOVING HEAD
    if (movement > 10) { 
        textSize(32);
        fill(255)
        text("you're moving your head!", windowWidth/2,windowHeight/2+50);
    }



// Check for mouse movement
   detectMouseMovement();

   // GET CURRENT POSITION
    
//USING P5.GEOLOCATION LIBRARY BY BEN MOREN
//navigator javascript


//GET CURRENT GEOLOCATION
navigator.geolocation.getCurrentPosition(setPosition);
//If true , the getCurrentPosition() method is called on the navigator.geolocation object. Retrieves the current position of the user. 
// setPos function  passed as an argument -- setPos callback function



}

//DRAW END


//---------------------------------------------------------------------------
//FUNCTIONS
//---------------------------------------------------------------------------


//UPDATE NOSE POSITION ARRAY
function getCurrentNosePos(array, noseX, noseY) {
    array.pop()
    array.push([noseX, noseY])
}

//CALCULATE MOVEMENT
function calculateMovement(previousPos, currentPos) {
   
    currNoseX = currentPos[33][0]; // Current frame nose position X
    currNoseY = currentPos[33][1]; // Current frame nose position Y

        // Calculate distance between current and previous nose positions
    let noseMovement = dist(previousPos[0][0], previousPos[0][1], currNoseX, currNoseY);
      
    return noseMovement
      }
//


//SET CURRENT GEOLOCATION
function setPosition(position) {
    var lat = position.coords.latitude.toFixed(4);
    var long = position.coords.longitude.toFixed(4);;
    background(0);
    fill(random(0,150))
    textSize(32);
    text("Your current position: " + (lat) + " " + (long), windowWidth / 2, windowHeight/2+10);
  }
//

//DETECT MOUSEMOVEMENT
function detectMouseMovement() {
    // Compare current mouse position with previous mouse position
    if (mouseX !== prevMouseX || mouseY !== prevMouseY) {
        // Mouse is moving
        text("you're moving your mouse!",mouseX,mouseY-20)
    }
    // Update previous mouse position
    prevMouseX = mouseX;
    prevMouseY = mouseY;
}

//RESIZE WINDOW
function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
  }