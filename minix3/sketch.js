
//define downloaded fonts to use later
let clockFont;
let boldFont;

// Variable to store the clock text
let clockText = ''; 


//Create the timer 
let startTime; // Variable to store the start time
let done = false; // Flag to check if 10 seconds have passed



// Make an array of emojis showing what else you could do with your time
let emojis = [];
let emojiOptions = ['💸', '💃', '⚽️', '🍽️', '🌸', '👨‍❤️‍💋‍👨', '🌴', '🍆', '🥇', '🎢', '🎁', '💘', '✈️']; // Array to store emoji options
let numEmojis = 30; // Number of emojis



function setup() {
   
    //create canvas
    createCanvas(windowWidth,windowHeight);

//map the fonts 
    clockFont = loadFont("digital-7.ttf");
    boldFont = loadFont("Angkor-Regular.ttf")


    
// Record the start time
    startTime = millis(); 

  //mesure angles in degrees 
    angleMode(DEGREES);


  // Initialize the array of emojis with random positions
for (let i = 0; i < numEmojis; i++) {
  // Generate random coordinates within the canvas boundaries
  let randomX = random(width);
  let randomY = random(20, height); // Ensure emojis are visible from the top
  
  // Select a random emoji from the options array
  let randomEmojiIndex = Math.floor(random(emojiOptions.length));
  let randomEmoji = emojiOptions[randomEmojiIndex];
  
  // Add the emoji object to the array with its properties
  emojis.push({ x: randomX, y: randomY, emoji: randomEmoji });
}
    

}



function draw() {

     // put drawing code here

  //yellow background
     background(255,255,39);


// THE EMOJIS

// Update and display each emoji



 // Update and display each emoji
for (let i = 0; i < numEmojis; i++) {
  // Update emoji position
  emojis[i].y += 1;

  // Display emoji
  fill(0); // Set the fill color
  textSize(50);
  textFont('Arial');
  text(emojis[i].emoji, emojis[i].x, emojis[i].y);

  // Reset emoji position if it goes out of the canvas
  if (emojis[i].y > height) {
      emojis[i].x = random(width);
      emojis[i].y = random(1, 200);
      emojis[i].emoji = random(emojiOptions); // Assign a new random emoji
  }
}

//THE CLOCK


// Calculate time spent on the page
let milliseconds = millis() - startTime;

// Convert milliseconds to hours, minutes, seconds, and milliseconds
let hours = Math.floor(milliseconds / 3600000);
milliseconds %= 3600000;
let minutes = Math.floor(milliseconds / 60000);
milliseconds %= 60000;
let seconds = Math.floor(milliseconds / 1000);
milliseconds %= 1000;

//Styling of clock
fill(0);
textSize(200);
textFont(clockFont);
textAlign(TOP, LEFT);
let fontSizeIncrease = map(seconds, 0, 1, 0, 12); // Adjust the second and range values as needed
textSize(100 + fontSizeIncrease);



 // Construct the clock text
    clockText =  
    hours.toString().padStart(2, '0') + ':' + 
    minutes.toString().padStart(2, '0') + ':' + 
    seconds.toString().padStart(2, '0') + '.' + 
    milliseconds.toString().slice(0, 2)


 // Display clock text
 text(clockText, 84, 550);


//QUOTE AT TOP


// quote text 1
textAlign(CENTER, CENTER);
textFont(boldFont);
textSize(65);
text('Time is precious - waste it wisely', 700, 100);

//quote text 2
textSize(50);
text('Waste it here?', 290, 180);

// text 1 
text('time spent waiting on page to load...', 600,600)



//Create fropper
let ms= millis();
let end= map(ms, 0, 500,0,360)
strokeWeight(8)
noFill();
//arc(x,y,d,d,start angle, end angle)
arc(mouseX,mouseY,50,50,0,end)



///Create last page
 // Check if 30 seconds have passed
 if (!done && seconds >= 10) {
    done = true;
  }

  // Display "Done" if 30 seconds have passed
  if (done) {
    textSize(50);
  
    background(0)
    fill(255)
    text('You are now done waiting... ', width / 2, 300);
    text('You just wasted 10.04 seconds of your life... ', width / 2, 400);

  }


 
  
}
  
