# MiniX1 // Waste of time...

please run the code here: https://signefn2000.gitlab.io/signefn2000/minix3

please view the full reposetory:
https://gitlab.com/signefn2000/signefn2000/-/tree/main/minix1?ref_type=heads


![billede](./jpg3.png)
![billede](./jpg2.png)


### **Reflections**

In my design i have focused less on the thropper and more on the underlying concept. 
In the process i thought a lot about how much time we spend, just waiting and looking at this little circle going round and round. Therefore i focused a lot on the p5.js functions involving time. I want to use the time people are spending waiting on the computer to make them reconsider weather or not they should be spending time on the computer anyways. Typically, when confronted with a loading animation like the fropper, users find themselves in a state of passive waiting, unable to engage with other tasks. This can create an illusion of time standing still as they watch the animation cycle endlessly. Through my design, I want to show that time continues to pass even as users remain fixated on the screen. 

My site counts the time you spent waiting looking at the thropper following your mouse. My thropper is too based on miliseconds using the map functions. The fropper has a fairly normal thropper look because i want it to be recognizable for people- This is important as i want them to consider how much time they are spending on the computer everytime they see the fropper. At the end the page goes black and shows how long you have been waiting for. This is important as it shows - quite literally - in black and white how much time we we loose looking into our screens. 




### **References**


[The Coding Train - Clock](https://www.youtube.com/watch?v=E4RyStef-gY&t=1s)